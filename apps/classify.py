import requests
import time
import os
import io
import sys
import json
import threading

from multiprocessing import Queue
from flask_restful import Resource, Api, reqparse
from flask import request
from werkzeug import secure_filename
from apps import app
from apps.image_resize import ImageResize
from apps.classification import Tensorflow

api = Api(app)

class Classify(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()

        # self.reqparse.add_argument('graph',
        #     type = str,
        #     required = True,
        #     help = '',
        #     location = 'form')

        self.reqparse.add_argument('imageUrl',
            type = str,
            required = False,
            help = 'Image Url to download',
            location = 'form')

        self.reqparse.add_argument('id',
            type = str,
            required = False,
            help = 'Unique ID for caller',
            location = 'form')

        # self.reqparse.add_argument('imageCrop',
        #     type = str,
        #     required = False,
        #     default = "none",
        #     help = 'right | left | none',
        #     location = 'form')

        super(Classify, self).__init__()
    
    def tensorFlowCall(self,tf, path, direction):
        results = tf.execute('graph-angle34', path)
        return {'dir': direction, 'results': results}
 
    # called by each thread
    def angle34Pass(self,q, tf, path, direction):
        q.put(self.tensorFlowCall(tf, path, direction))

    def pickFinalCategory(self,leftCat, rightCat):
        if (leftCat == "34 side" and rightCat == "34 front"):
            usedCat = "front 34 driver"
        elif (leftCat == "34 side" and rightCat == "34 rear"):
            usedCat = "rear 34 passenger"
        elif (leftCat == "34 front" and rightCat == "34 side"):
            usedCat = "front 34 passenger"
        elif (leftCat == "34 rear" and rightCat == "34 side"):
            usedCat = "rear 34 driver"
        else:
            usedCat = ''
            
        return usedCat

    def isSide(self,cat):
        return cat == "34 side"

    def angle34Logic(self,leftOutput,rightOutput):
        sortedLeft = sorted(leftOutput['categories'], key=lambda k: (-float(k["confscore"])))
        sortedRight = sorted(rightOutput['categories'], key=lambda k: (-float(k["confscore"])))
        
        leftTopScore = sortedLeft[0]
        rightTopScore = sortedRight[0]

        leftTopScore2 = sortedLeft[1]
        rightTopScore2 = sortedRight[1]

        #index 0
        winningCatLeft = leftTopScore['name']
        winningCatRight = rightTopScore['name']
        winningCatLeftScore = float(leftTopScore['confscore'])
        winningCatRightScore = float(rightTopScore['confscore'])

        #index 1
        winningCatLeft2 = leftTopScore2['name']
        winningCatRight2 = rightTopScore2['name']
        winningCatLeftScore2 = float(leftTopScore2['confscore'])
        winningCatRightScore2 = float(rightTopScore2['confscore'])

        cat1 = "none"
        if (self.isSide(winningCatLeft) != self.isSide(winningCatRight)):
            # If left and right are not from the same group then use the original categories

            # Combinations covered:
            # Front Side, Rear Side, Side Front, Side Rear
            cat1 = self.pickFinalCategory(winningCatLeft, winningCatRight)  
            confScore = (winningCatLeftScore + winningCatRightScore) / 2                         
        else:
            # Combinations covered:
            # Front Front
            # Front Rear
            # Rear Rear
            # Rear Front
            # Side Side

            if (winningCatLeft == winningCatRight):
                # If left and right are the same, find the second winning category which of different group
                # Combinations covered:
                # Front Front, Rear Rear, Side Side    
                if ((self.isSide(winningCatLeft) != self.isSide(winningCatRight2)) and (self.isSide(winningCatLeft2) != self.isSide(winningCatRight))):
                
                    # If either second winning category left or right are valid, then pick the one with highest score
                    if (winningCatLeftScore2 > winningCatRightScore2):
                        cat1 = self.pickFinalCategory(winningCatLeft2, winningCatRight)
                        confScore = (winningCatLeftScore2 + winningCatRightScore) / 2                         
                    else:
                        cat1 = self.pickFinalCategory(winningCatLeft, winningCatRight2)                                        
                        confScore = (winningCatLeftScore + winningCatRightScore2) / 2                         
                elif (self.isSide(winningCatLeft) != self.isSide(winningCatRight2)):
                    cat1 = self.pickFinalCategory(winningCatLeft, winningCatRight2)
                    confScore = (winningCatLeftScore + winningCatRightScore2) / 2                         
                elif (self.isSide(winningCatLeft2) != self.isSide(winningCatRight)):
                    cat1 = self.pickFinalCategory(winningCatLeft2, winningCatRight)
                    confScore = (winningCatLeftScore2 + winningCatRightScore) / 2                         
                else:
                    # We are screwed, classified as misc
                    cat1 = "misc"
            else:
                # Combinations covered:
                # Front Rear
                # Rear Front
                # Pick the second winning category which has side
                if (self.isSide(winningCatLeft2) and self.isSide(winningCatRight2)):
                
                    # Either left or right is side, then choose one with highest score
                    if (winningCatLeftScore2 > winningCatRightScore2):
                        cat1 = self.pickFinalCategory(winningCatLeft2, winningCatRight)
                        confScore = (winningCatLeftScore2 + winningCatRightScore) / 2                         
                    else:
                        cat1 = self.pickFinalCategory(winningCatLeft, winningCatRight2)                                        
                        confScore = (winningCatLeftScore + winningCatRightScore2) / 2                         
                elif (self.isSide(winningCatLeft2)):
                    cat1 = self.pickFinalCategory(winningCatLeft2, winningCatRight)                                    
                    confScore = (winningCatLeftScore2 + winningCatRightScore) / 2                         
                elif (self.isSide(winningCatRight2)):
                    cat1 = self.pickFinalCategory(winningCatLeft, winningCatRight2)                                    
                    confScore = (winningCatLeftScore + winningCatRightScore2) / 2                         
                else:
                    # We are screwed, classified as misc
                    cat1 = "misc"
                    confScore = (winningCatLeftScore + winningCatRightScore) / 2
            
        return {"categories": [{"name": cat1,"confscore": confScore}]}
    
    def post(self):
        const_sideview = 'side view'
        const_front34 = 'front 34'
        const_rear34 = 'rear 34'
        const_side34 = 'side 34'
        const_graphfirst = 'graph-first'
        const_graphside = 'graph-side'
        const_graphangle34 = 'graph-angle34'

        graphUsed = 'first'
        postStart = time.time()
 
        args = self.reqparse.parse_args()

        start = time.time()
        
        if (args.imageUrl):
            response = requests.get(args.imageUrl, stream=True)
            image = io.BytesIO(response.content)
        else:
            image = request.files['image']
            image.seek(0)
            
        downloadTimeElapsed = (time.time() - start)

        start = time.time()
        imgResize = ImageResize()
        imgOutputPath = imgResize.resize(image, 'none')
        
        resizeTimeElapsed = (time.time() - start)

        start = time.time()
        tf = Tensorflow()
        output = tf.execute(const_graphfirst, imgOutputPath)
        sortedScore = sorted(output['categories'], key=lambda k: (-float(k["confscore"])))
        firstTopScore = (sortedScore[:1] or [None])[0]

        #Side view classification
        if (firstTopScore['name'] == const_sideview):
            os.remove(imgOutputPath)
            imgOutputPath = imgResize.resize(image, 'left')
            output = tf.execute(const_graphside, imgOutputPath)
            os.remove(imgOutputPath)
            graphUsed = 'side'
        #Angle 34 classification    
        elif (firstTopScore['name'] == const_front34 or firstTopScore['name'] == const_rear34):
            os.remove(imgOutputPath)
            imgLeftOutputPath = imgResize.resize(image, 'left')
            imgRightOutputPath = imgResize.resize(image, 'right')
            graphUsed = 'angle34'
            #Adding a queue because two tensorflow is running
            q = Queue()

            t = threading.Thread(target=self.angle34Pass, args = (q,tf,imgLeftOutputPath, 'left'))
            t.daemon = True
            t.start()

            t = threading.Thread(target=self.angle34Pass, args = (q,tf,imgRightOutputPath, 'right'))
            t.daemon = True
            t.start()

            #Include two images in output
            imgOutputPath = imgLeftOutputPath + '|' + imgRightOutputPath
            firstOutput = q.get()
            secondOutput = q.get()
            if (firstOutput['dir'] == 'left'):
                leftOutput =  firstOutput['results']
                rightOutput =  secondOutput['results']
            else: 
                leftOutput =  secondOutput['results']
                rightOutput = firstOutput['results']

            output = self.angle34Logic(leftOutput,rightOutput)
            os.remove(imgRightOutputPath)
            os.remove(imgLeftOutputPath)

        else:
            os.remove(imgOutputPath)
        
        classifyTimeElapsed = (time.time() - start)
       
        postEnd = time.time()

        return {
            'id': args.id,
            'classifyOutput': output,
            'classifyStats': {
                'classifyTimeElapsedMs': ("%.2f" % float(classifyTimeElapsed * 1000)),
                'totalTimeElapsedMs': ("%.2f" % float((postEnd - postStart) * 1000)),
                'downloadTimeElapsedMs': ("%.2f" % float(downloadTimeElapsed * 1000)),
                'resizeTimeElapsedMs': ("%.2f" % float(resizeTimeElapsed * 1000)),
                'resizeImagePath': imgOutputPath,
                'graphUsed': graphUsed
            }
        }

api.add_resource(Classify, '/classify', endpoint = 'classify')
